========
 Preface
========

For thousands of years, people have been saying that the world is changing
and will never again be the same. Yet the profound changes happening today
are different, because they result from a specific technological development

It is now possible, in principle, to remember everything that anyone says,
writes, sings, draws, or photographs. *Everything*. If digitized, the world has
enough disks and memory chips to save it all, for as long as civilization can
keep producing computers and disk drives. Global computer networks can
make it available to everywhere in the world, almost instantly. And computers
are powerful enough to extract meaning from all that information, to find
patterns and make connections in the blink of an eye.

In centuries gone by, others may have dreamed these things could happen,
in utopian fantasies or in nightmares. But now they are happening. We are
living in the middle of the changes, and we can see the changes happening.
But we don’t know how things will turn out.

Right now, governments and the other institutions of human societies are
deciding how to use the new possibilities. Each of us is participating as we
make decisions for ourselves, for our families, and for people we work with.
Everyone needs to know how their world and the world around them is
changing as a result of this explosion of digital information. Everyone should
know how the decisions will affect their lives, and the lives of their children
and grandchildren and everyone who comes after.

That is why we wrote this book.

Each of us has been in the computing field for more than 40 years. The
book is the product of a lifetime of observing and participating in the changes
it has brought. Each of us has been both a teacher and a learner in the field.
This book emerged from a general education course we have taught at
Harvard, but it is not a textbook. We wrote this book to share what wisdom
we have with as many people as we can reach. We try to paint a big picture,
with dozens of illuminating anecdotes as the brushstrokes. We aim to entertain
you at the same time as we provoke your thinking.

You can read the chapters in any order. The Appendix is a self-contained
explanation of how the Internet works. You don’t need a computer to read
this book. But we would suggest that you use one, connected to the Internet,
to explore any topic that strikes your curiosity or excites your interest. Don’t
be afraid to type some of the things we mention into your favorite search
engine and see what comes up. We mention many web sites, and give their
complete descriptors, such as ``bitsbook.com``, which happens to be the site for
this book itself. But most of the time, you should be able to find things more
quickly by searching for them. There are many valuable public information
sources and public interest groups where you can learn more, and can participate
in the ongoing global conversation about the issues we discuss.

We offer some strong opinions in this book. If you would like to react to
what we say, please visit the book’s web site for an ongoing discussion.

Our picture of the changes brought by the digital explosion is drawn
largely with reference to the United States and its laws and culture, but the
issues we raise are critical for citizens of all free societies, and for all people
who hope their societies will become freer. 

Cambridge, Massachusetts

January 2008 