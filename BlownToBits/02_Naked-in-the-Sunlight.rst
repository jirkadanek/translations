=======================
 Naked in the Sunlight
=======================
---------------------------------
 Privacy Lost, Privacy Abandoned
---------------------------------

1984 Is Here, and We Like It
============================

On July 7, 2005, London was shaken as suicide bombers detonated four
explosions, three on subways and one on a double-decker bus. The attack on
the transit system was carefully timed to occur at rush hour, maximizing its
destructive impact. 52 people died and 700 more were injured.

Security in London had already been tight. The city was hosting the G8
Summit, and the trial of fundamentalist cleric Abu Hamza al-Masri had just
begun. Hundreds of thousands of surveillance cameras hadn’t deterred the
terrorist act, but the perpetrators were caught on camera. Their pictures were
sent around the world instantly. Working from 80,000 seized tapes, police
were able to reconstruct a reconnaissance trip the bombers had made two
weeks earlier.

George Orwell’s 1984 was published in 1948. Over the subsequent years,
the book became synonymous with a world of permanent surveillance, a society
devoid of both privacy and freedom:

   …there seemed to be no color in anything except the posters that were
   plastered everywhere. The black-mustachio’d face gazed down from
   every commanding corner. There was one on the house front immediately
   opposite. BIG BROTHER IS WATCHING YOU …
   
The real 1984 came and went nearly a quarter century ago. Today, Big
Brother’s two-way telescreens would be amateurish toys. Orwell’s imagined
London had cameras everywhere. His actual city now has at least half a million.
Across the UK, there is one surveillance camera for every dozen people.
The average Londoner is photographed hundreds of times a day by electronic
eyes on the sides of buildings and on utility poles.

Yet there is much about the digital world that Orwell did not imagine. He
did not anticipate that cameras are far from the most pervasive of today’s
tracking technologies. There are dozens of other kinds of data sources, and
the data they produce is retained and analyzed. Cell phone companies know
not only what numbers you call, but where you have carried your phone.
Credit card companies know not only where you spent your money, but what
you spent it on. Your friendly bank keeps electronic records of your transactions
not only to keep your balance right, but because it has to tell the government
if you make huge withdrawals. The digital explosion has scattered
the bits of our lives everywhere: records of the clothes we wear, the soaps we
wash with, the streets we walk, and the cars we drive and where we drive
them. And although Orwell’s Big Brother had his cameras, he didn’t have
search engines to piece the bits together, to find the needles in the haystacks.
Wherever we go, we leave digital footprints, while computers of staggering
capacity reconstruct our movements from the tracks. Computers re-assemble
the clues to form a comprehensive image of who we are, what we do, where
we are doing it, and whom we are discussing it with.

Perhaps none of this would have surprised Orwell. Had he known about
electronic miniaturization, he might have guessed that we would develop an
astonishing array of tracking technologies. Yet there is something more fundamental
that distinguishes the world of 1984 from the actual world of today.
We have fallen in love with this always-on world. We accept our loss of privacy
in exchange for efficiency, convenience, and small price discounts.
According to a 2007 Pew/Internet Project report, “60% of Internet users say
they are not worried about how much information is available about them
online.” Many of us publish and broadcast the most intimate moments of our
lives for all the world to see, even when no one requires or even asks us to
do so. 55% of teenagers and 20% of adults have created profiles on social
networking web sites. A third of the teens with profiles, and half the adults,
place no restrictions on who can see them

In Orwell’s imagined London, only O’Brien and other members of the Inner
Party could escape the gaze of the telescreen. For the rest, the constant gaze
was a source of angst and anxiety. Today, we willingly accept the gaze. We
either don’t think about it, don’t know about it, or feel helpless to avoid it
except by becoming hermits. We may even judge its benefits to outweigh its
risks. In Orwell’s imagined London, like Stalin’s actual Moscow, citizens spied
on their fellow citizens. Today, we can all be Little Brothers, using our search
engines to check up on our children,
our spouses, our neighbors, our colleagues,
our enemies, and our
friends. More than half of all adult
Internet users have done exactly
that.

.. sidebar:: PUBLIC ORGANIZATIONS INVOLVED IN DEFENDING PRIVACY

    Existing organizations have focused
    on privacy issues in recent years,
    and new ones have sprung up.
    In the U.S., important forces are
    the American Civil Liberties Union
    (ACLU, ``www.aclu.org``), the
    Electronic Privacy Information
    Center (EPIC, epic.org), the
    Center for Democracy and
    Technology (CDT, ``www.cdt.org``),
    and the Electronic Frontier
    Foundation (``www.eff.org``).

The explosive growth in digital
technologies has radically altered
our expectations about what will be
private and shifted our thinking
about what *should* be private.
Ironically, the notion of privacy has
become fuzzier at the same time as
the secrecy-enhancing technology of
encryption has become widespread.
Indeed, it is remarkable that we no
longer blink at intrusions that a decade ago would have seemed shocking.
Unlike the story of secrecy, there was no single technological event that
caused the change, no privacy-shattering breakthrough—only a steady
advance on several technological fronts that ultimately passed a tipping point.

Many devices got cheaper, better, and smaller. Once they became useful
consumer goods, we stopped worrying about their uses as surveillance
devices. For example, if the police were the only ones who had cameras in
their cell phones, we would be alarmed. But as long as we have them too, so
we can send our friends funny pictures from parties, we don’t mind so much
that others are taking pictures of us. The social evolution that was supported
by consumer technologies in turn made us more accepting of new enabling
technologies; the social and technological evolutions have proceeded hand in
hand. Meanwhile, international terrorism has made the public in most democracies
more sympathetic to intrusive measures intended to protect our security.
With corporations trying to make money from us and the government
trying to protect us, civil libertarians are a weak third voice when they warn
that we may not want others to know so much about us.

So we tell the story of privacy in stages. First, we detail the enabling technologies,
the devices and computational processes that have made it easy and
convenient for us to lose our privacy—some of them familiar technologies,
and some a bit more mysterious. We then turn to an analysis of how we have
lost our privacy, or simply abandoned it. Many privacy-shattering things
have happened to us, some with our cooperation and some not. As a result,
the sense of personal privacy is very different today than it was two decades
ago. Next, we discuss the social changes that have occurred—cultural shifts
that were facilitated by the technological diffusion, which in turn made new
technologies easier to deploy. And finally we turn to the big question: What
does privacy even mean in the digitally exploded world? Is there any hope of
keeping anything private when everything is bits, and the bits are stored,
copied, and moved around the world in an instant? And if we can’t—or
won’t—keep our personal information to ourselves anymore, how can we
make ourselves less vulnerable to the downsides of living in such an exposed
world? Standing naked in the sunlight, is it still possible to protect ourselves
against ills and evils from which our privacy used to protect us?

Footprints and Fingerprints
===========================

As we do our daily business and lead our private lives, we leave footprints
and fingerprints. We can see our footprints in mud on the floor and in the
sand and snow outdoors. We would not be surprised that anyone who went
to the trouble to match our shoes to our footprints could determine, or guess,
where we had been. Fingerprints are
different. It doesn’t even occur to us
that we are leaving them as we open
doors and drink out of tumblers.
Those who have guilty consciences
may think about fingerprints and
worry about where they are leaving
them, but the rest of us don’t.

.. sidebar:: THE UNWANTED GAZE

    *The Unwanted Gaze* by Jeffrey
    Rosen (Vintage, 2001) details many
    ways in which the legal system has
    contributed to our loss of privacy.

In the digital world, we all leave both electronic footprints and electronic
fingerprints—data trails we leave intentionally, and data trails of which we
are unaware or unconscious. The identifying data may be useful for forensic
purposes. Because most of us don’t consider ourselves criminals, however, we
tend not to worry about that. What we don’t think about is that the various
small smudges we leave on the digital landscape may be useful to someone
else—someone who wants to use the data we left behind to make money or to
get something from us. It is therefore important to understand how and where
we leave these digital footprints and fingerprints.

Smile While We Snap!
--------------------