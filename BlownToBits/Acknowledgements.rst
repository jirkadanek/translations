==================
 Acknowledgements
==================

While we take full responsibility for any errors in the book, we owe thanks
to a great many others for any enlightenment it may provide. Specifically, we
are grateful to the following individuals, who commented on parts of the
book while it was in draft or provided other valuable assistance: Lynn
Abelson, Meg Ausman, Scott Bradner, Art Brodsky, Mike Carroll, Marcus
Cohn, Frank Cornelius, Alex Curtis, Natasha Devroye, David Fahrenthold,
Robert Faris, Johann-Christoph Freytag, Wendy Gordon, Tom Hemnes, Brian
LaMacchia, Marshall Lerner, Anne Lewis, Elizabeth Lewis, Jessica Litman,
Lory Lybeck, Fred vonLohmann, Marlyn McGrath, Michael Marcus, Michael
Mitzenmacher, Steve Papa, Jonathan Pearce, Bradley Pell, Les Perelman,
Pamela Samuelson, Jeff Schiller, Katie Sluder, Gigi Sohn, Debora Spar,
René Stein, Alex Tibbetts, Susannah Tobin, Salil Vadhan, David Warsh,
Danny Weitzner, and Matt Welsh.