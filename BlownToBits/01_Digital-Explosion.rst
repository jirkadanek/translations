===================
 Digital Explosion
===================
--------------------------------------------
 Why Is It Happening, and What Is at Stake?
--------------------------------------------

On September 19, 2007, while driving alone near Seattle on her way to work,
Tanya Rider went off the road and crashed into a ravine.* For eight days, she
was trapped upside down in the wreckage of her car. Severely dehydrated and
suffering from injuries to her leg and shoulder, she nearly died of kidney failure.
Fortunately, rescuers ultimately found her. She spent months recuperating
in a medical facility. Happily, she was able to go home for Christmas.

Tanya’s story is not just about a woman, an accident, and a rescue. It is a
story about bits—the zeroes and ones that make up all our cell phone conversations,
bank records, and everything else that gets communicated or stored
using modern electronics.

Tanya was found because cell phone companies keep records of cell phone
locations. When you carry your cell phone, it regularly sends out a digital
“ping,” a few bits conveying a “Here I am!” message. Your phone keeps “pinging”
as long as it remains turned on. Nearby cell phone towers pick up the
pings and send them on to your cellular service provider. Your cell phone
company uses the pings to direct your incoming calls to the right cell phone
towers. Tanya’s cell phone company, Verizon, still had a record of the last
location of her cell phone, even after the phone had gone dead. That is how
the police found her.

So why did it take more than a week?

If a woman disappears, her husband can’t just make the police find her by
tracing her cell phone records. She has a privacy right, and maybe she has
good reason to leave town without telling her husband where she is going. In
Tanya’s case, her bank account showed some activity (more bits!) after her
disappearance, and the police could not classify her as a “missing person.” In
fact, that activity was by her husband. Through some misunderstanding, the
police thought he did not have access to the account. Only when the police
suspected Tanya’s husband of involvement in her disappearance did they
have legal access to the cell phone records. Had they continued to act on the
true presumption that he was blameless, Tanya might never have been found.

New technologies interacted in an odd way with evolving standards of privacy,
telecommunications, and criminal law. The explosive combination
almost cost Tanya Rider her life. Her story is dramatic, but every day we
encounter unexpected consequences of data flows that could not have happened
a few years ago.

When you have finished reading this book, you should see the world in a
different way. You should hear a story from a friend or on a newscast and say
to yourself, “that’s really a bits story,” even if no one mentions anything digital.
The movements of physical objects and the actions of flesh and blood
human beings are only the surface. To understand what is really going on, you
have to see the virtual world, the eerie flow of bits steering the events of life.
This book is your guide to this new world.

The Explosion of Bits, and Everything Else
==========================================

The world changed very suddenly. Almost everything is stored in a computer
somewhere. Court records, grocery purchases, precious family photos, pointless
radio programs…. Computers contain a lot of stuff that isn’t useful today
but somebody thinks might someday come in handy. It is all being reduced
to zeroes and ones—“bits.” The bits are stashed on disks of home computers
and in the data centers of big corporations and government agencies. The
disks can hold so many bits that there is no need to pick and choose what
gets remembered.

So much digital information, misinformation, data, and garbage is being
squirreled away that most of it will be seen only by computers, never by
human eyes. And computers are getting better and better at extracting meaning
from all those bits—finding patterns that sometimes solve crimes and
make useful suggestions, and sometimes reveal things about us we did not
expect others to know.

The March 2008 resignation of Eliot Spitzer as Governor of New York is a
bits story as well as a prostitution story. Under anti-money laundering (AML)
rules, banks must report transactions of more than $10,000 to federal regulators.
None of Spitzer’s alleged payments reached that threshold, but his
bank’s computer found that transfers of smaller sums formed a suspicious
pattern. The AML rules exist to fight terrorism and organized crime. But while
the computer was monitoring small banking transactions in search of
big-time crimes, it exposed a simple payment for services rendered that
brought down the Governor.

Once something is on a computer, it can replicate and move around the
world in a heartbeat. Making a million perfect copies takes but an instant—
copies of things we want everyone in the world to see, and also copies of
things that weren’t meant to be copied at all. 

The digital explosion is changing the world as much as printing once did—
and some of the changes are catching us unaware, blowing to bits our
assumptions about the way the world works. 

When we observe the digital explosion at all, it can seem benign, amusing,
or even utopian. Instead of sending prints through the mail to Grandma,
we put pictures of our children on a photo album web site such as Flickr. Then
not only can Grandma see them—so can Grandma’s friends and anyone else.
So what? They are cute and harmless. But suppose a tourist takes a vacation
snapshot and you just happen to appear in the background, at a restaurant
where no one knew you were dining. If the tourist uploads his photo, the
whole world could know where you were, and when you were there.

Data leaks. Credit card records are supposed to stay locked up in a data
warehouse, but escape into the hands of identity thieves. And we sometimes
give information away just because we get something back for doing so. A
company will give you free phone calls to anywhere in the world—if you
don’t mind watching ads for the products its computers hear you talking
about.

And those are merely things that are happening today. The explosion, and
the social disruption it will create, have barely begun.

We already live in a world in which there is enough memory *just in digital
cameras* to store every word of every book in the Library of Congress a
hundred times over. So much email is being sent that it could transmit the
full text of the Library of Congress in ten minutes. Digitized pictures and
sounds take more space than words, so emailing all the images, movies, and
sounds might take a year—but that is just today. The explosive growth is still
happening. Every year we can store more information, move it more quickly,
and do far more ingenious things with it than we could the year before.

So much disk storage is being produced every year that it could be used to
record a page of information, every minute or two, about *you and every other
human being on earth*. A remark made long ago can come back to haunt a
political candidate, and a letter jotted quickly can be a key discovery for a
biographer. Imagine what it would mean to record every word every human
being speaks or writes in a lifetime. The technological barrier to that has
already been removed: There is enough storage to remember it all. Should
any social barrier stand in the way?

Sometimes things seem to work both better and worse than they used to.
A “public record” is now *very* public—before you get hired in Nashville,
Tennessee, your employer can figure out if you were caught ten years ago
taking an illegal left turn in Lubbock, Texas. The old notion of a “sealed court
record” is mostly a fantasy in a world where any tidbit of information is
duplicated, cataloged, and moved around endlessly. With hundreds of TV and
radio stations and millions of web sites, Americans love the variety of news
sources, but are still adjusting uncomfortably to the displacement of more
authoritative sources. In China, the situation is reversed: The technology creates
greater government control of the information its citizens receive, and
better tools for monitoring their behavior. 

This book is about how the digital explosion is changing everything. It
explains the technology itself—why it creates so many surprises and why
things often don’t work the way we expect them to. It is also about things the
information explosion is destroying: old assumptions about our privacy,
about our identity, and about who is in control of our lives. It’s about how
we got this way, what we are losing, and what remains that society still has
a chance to put right. The digital explosion is creating both opportunities and
risks. Many of both will be gone in a decade, settled one way or another.
Governments, corporations, and other authorities are taking advantage of the
chaos, and most of us don’t even see it happening. Yet we all have a stake in
the outcome. Beyond the science, the history, the law, and the politics, this
book is a wake-up call. The forces shaping your future are digital, and you
need to understand them. 

The Koans of Bits
=================

Bits behave strangely. They travel almost instantaneously, and they take
almost no space to store. We have to use physical metaphors to make them
understandable. We liken them to dynamite exploding or water flowing. We
even use social metaphors for bits. We talk about two computers agreeing on
some bits, and about people using burglary tools to steal bits. Getting the right
metaphor is important, but so is knowing the limitations of our metaphors. An
imperfect metaphor can mislead as much as an apt metaphor can illuminate.

.. sidebar:: CLAUDE SHANNON

    .. image:: assets/shannon2_lg.jpeg
        :width: 25%
        :align: left
        :alt: Claude Shannon

    Claude Shannon (1916–2001) is the undisputed
    founding figure of information and
    communication theory. While working at Bell
    Telephone Laboratories after the Second
    World War, he wrote the seminal paper, “A
    mathematical theory of communication,”
    which foreshadowed much of the subsequent
    development of digital technologies.
    Published in 1948, this paper gave birth to
    the now-universal realization that the bit is
    the natural unit of information, and to the
    use of the term.

    Alcatel-Lucent, ``http:www.bell-labs.com/news/2001/february/26/shannon2_lg.jpeg``.

We offer seven truths about bits. We call them “koans” because they are
paradoxes, like the Zen verbal puzzles that provoke meditation and enlightenment.
These koans are oversimplifications and over-generalizations. They
describe a world that is developing but hasn’t yet fully emerged. But even
today they are truer than we often realize. These themes will echo through
our tales of the digital explosion

Koan 1: It’s All Just Bits
--------------------------

Your computer successfully creates the illusion that it contains photographs,
letters, songs, and movies. All it really contains is bits, lots of them, patterned
in ways you can’t see. Your computer was designed to store just bits—all the
files and folders and different kinds of data are illusions created by computer
programmers. When you send an email containing a photograph, the computers
that handle your message as it flows through the Internet have no idea
that what they are handling is part text and part graphic. Telephone calls are
also just bits, and that has helped create competition—traditional phone companies,
cell phone companies, cable TV companies, and Voice over IP (VoIP)
service providers can just shuffle bits around to each other to complete calls.
The Internet was designed to handle just bits, not emails or attachments,
which are inventions of software engineers. We couldn’t live without those
more intuitive concepts, but they are artifices. Underneath, it’s all just bits.

This koan is more consequential than you might think. Consider the story
of Naral Pro-Choice America and Verizon Wireless. Naral wanted to form a
text messaging group to send alerts to its members. Verizon decided not to
allow it, citing the “controversial or unsavory” things the messages might
contain. Text message alert groups for political candidates it would allow, but
not for political causes it deemed controversial. Had Naral simply wanted
telephone service or an 800 number, Verizon would have had no choice.
Telephone companies were long ago declared “common carriers.” Like railroads,
phone companies are legally prohibited from picking and choosing
customers from among those who want their services. In the bits world, there
is no difference between a text message and a wireless phone call. It’s all just
bits, traveling through the air by radio waves. But the law hasn’t caught up
to the technology. It doesn’t treat all bits the same, and the common carriage
rules for voice bits don’t apply to text message bits.

Verizon backed down in the case
of Naral, but not on the principle. A
phone company can do whatever it
thinks will maximize its profits in
deciding whose messages to distribute.
Yet no sensible engineering distinction
can be drawn between text
messages, phone calls, and any other
bits traveling through the digital airwaves.

.. sidebar:: EXCLUSIVE AND RIVALROUS

    Economists would say that bits,
    unless controlled somehow, tend to
    be non-exclusive (once a few people
    have them, it is hard to keep
    them from others) and nonrivalrous
    (when someone gets them
    from me, I don’t have any less). In a
    letter he wrote about the nature of
    ideas, Thomas Jefferson eloquently
    stated both properties. If nature
    has made any one thing less susceptible
    than all others of exclusive
    property, it is the action of the
    thinking power called an idea,
    which an individual may exclusively
    possess as long as he keeps
    it to himself; but the moment it is
    divulged, it forces itself into the
    possession of every one, and the
    receiver cannot dispossess himself
    of it. Its peculiar character, too, is
    that no one possesses the less,
    because every other possesses the
    whole of it.

Koan 2: Perfection Is Normal 
----------------------------

To err is human. When books were
laboriously transcribed by hand, in
ancient scriptoria and medieval
monasteries, errors crept in with
every copy. Computers and networks
work differently. Every copy is perfect.
If you email a photograph to a
friend, the friend won’t receive a
fuzzier version than the original. The
copy will be identical, down to the
level of details too small for the eye
to see.

Computers do fail, of course.
Networks break down too. If the
power goes out, nothing works at all. So the statement that copies are normally
perfect is only relatively true. Digital copies are perfect only to the
extent that they can be communicated at all. And yes, it is possible in theory
that a single bit of a big message will arrive incorrectly. But networks don’t
just pass bits from one place to another. They check to see if the bits seem to
have been damaged in transit, and correct them or retransmit them if they
seem incorrect. As a result of these error detection and correction mechanisms,
the odds of an actual error—a character being wrong in an email, for
example—are so low that we would be wiser to worry instead about a meteor
hitting our computer, improbable though precision meteor strikes may be.

The phenomenon of perfect copies has drastically changed the law, a story
told in Chapter 6, “Balance Toppled.” In the days when music was distributed
on audio tape, teenagers were not prosecuted for making copies of songs,
because the copies weren’t as good as the originals, and copies of copies
would be even worse. The reason that thousands of people are today receiving
threats from the music and movie industries is that their copies are perfect—not
just as good as the original, but identical to the original, so that
even the notion of “original” is meaningless. The dislocations caused by file
sharing are not over yet. The buzzword of the day is “intellectual property.”
But bits are an odd kind of property. Once I release them, everybody has
them. And if I give you my bits, I don’t have any fewer.

Koan 3: There Is Want in the Midst of Plenty
--------------------------------------------

Vast as world-wide data storage is today, five years from now it will be ten
times as large. Yet the information explosion means, paradoxically, the loss
of information that is not online. One of us recently saw a new doctor at a
clinic he had been using for decades. She showed him dense charts of his
blood chemistry, data transferred from his home medical device to the clinic’s
computer—more data than any specialist could have had at her disposal five
years ago. The doctor then asked whether he had ever had a stress test and
what the test had shown. Those records should be all there, the patient
explained, in the medical file. But it was in the paper file, to which the doctor
did not have access. It wasn’t in the *computer’s* memory, and the patient’s
memory was being used as a poor substitute. The old data might as well not
have existed at all, since it wasn’t digital.

Even information that exists in digital form is useless if there are no
devices to read it. The rapid progress of storage engineering has meant that
data stored on obsolete devices effectively ceases to exist. In Chapter 3,
“Ghosts in the Machine,” we shall see how a twentieth-century update of the
eleventh-century British Domesday Book was useless by the time it was only
a sixtieth the age of the original.

Or consider search, the subject of Chapter 4, “Needles in the Haystack.” At
first, search engines such as Google and Yahoo! were interesting conveniences,
which a few people used for special purposes. The growth of the World
Wide Web has put so much information online that search engines are for
many people the first place to look for something, before they look in books
or ask friends. In the process, appearing prominently in search results has
become a matter of life or death for businesses. We may move on to purchase
from a competitor if we can’t find the site we wanted in the first page or two
of results. We may assume something didn’t happen if we can’t find it quickly
in an online news source. If it can’t be found—and found quickly—it’s just as
though it doesn’t exist at all.

Koan 4: Processing Is Power
---------------------------

.. sidebar:: MOORE’S LAW

    Gordon Moore, founder of Intel
    Corporation, observed that the
    density of integrated circuits
    seemed to double every couple of
    years. This observation is referred
    to as “Moore’s Law.” Of course, it is
    not a natural law, like the law of
    gravity. Instead, it is an empirical
    observation of the progress of
    engineering and a challenge to
    engineers to continue their innovation.
    In 1965, Moore predicted that
    this exponential growth would
    continue for quite some time. That
    it has continued for more than 40
    years is one of the great marvels of
    engineering. No other effort in history
    has sustained anything like
    this growth rate.

The speed of a computer is usually
measured by the number of basic
operations, such as additions, that
can be performed in one second. The
fastest computers available in the
early 1940s could perform about
five operations per second. The
fastest today can perform about a
trillion. Buyers of personal computers
know that a machine that seems
fast today will seem slow in a year
or two.

For at least three decades, the
increase in processor speeds was
exponential. Computers became
twice as fast every couple of years.
These increases were one consequence
of “Moore’s Law” (see sidebar).

Since 2001, processor speed has
not followed Moore’s Law; in fact,
processors have hardly grown faster
at all. But that doesn’t mean that computers won’t continue to get faster. New
chip designs include multiple processors on the same chip so the work can be
split up and performed in parallel. Such design innovations promise to
achieve the same effect as continued increases in raw processor speed. And
the same technology improvements that make computers faster also make
them cheaper.

The rapid increase in processing power means that inventions move out of
labs and into consumer goods very quickly. Robot vacuum cleaners and selfparking
vehicles were possible in theory a decade ago, but now they have
become economically feasible. Tasks that today seem to require uniquely
human skills are the subject of research projects in corporate or academic laboratories.
Face recognition and voice recognition are poised to bring us new
inventions, such as telephones that know who is calling and surveillance
cameras that don’t need humans to watch them. The power comes not just
from the bits, but from being able to do things with the bits.

Koan 5: More of the Same Can Be a Whole New Thing
-------------------------------------------------

Explosive growth is exponential growth—doubling at a steady rate. Imagine
earning 100% annual interest on your savings account—in 10 years, your
money would have increased more than a thousandfold, and in 20 years,
more than a millionfold. A more reasonable interest rate of 5% will hit the
same growth points, just 14 times more slowly. Epidemics initially spread
exponentially, as each infected individual infects several others.

When something grows exponentially, for a long time it may seem not to
be changing at all. If we don’t watch it steadily, it will seem as though something
discontinuous and radical occurred while we weren’t looking.

That is why epidemics at first go unnoticed, no matter how catastrophic
they may be when full-blown. Imagine one sick person infecting two healthy
people, and the next day each of those two infects two others, and the next
day after that each of those four infects two others, and so on. The number
of newly infected each day grows from two to four to eight. In a week, 128
people come down with the disease in a single day, and twice that number
are now sick, but in a population of ten million, no one notices. Even after
two weeks, barely three people in a thousand are sick. But after another week,
40% of the population is sick, and society collapses.

Exponential growth is actually smooth and steady; it just takes very little
time to pass from unnoticeable change to highly visible. Exponential growth
of anything can suddenly make the world look utterly different than it had
been. When that threshold is passed, changes that are “just” quantitative can
look qualitative.

Another way of looking at the apparent abruptness of exponential
growth—its explosive force—is to think about how little lead time we have to
respond to it. Our hypothetical epidemic took three weeks to overwhelm the
population. At what point was it only a half as devastating? The answer is
not “a week and a half.” The answer is *on the next to last day*. Suppose it took
a week to develop and administer a vaccine. Then noticing the epidemic after
a week and a half would have left ample time to prevent the disaster. But that
would have required understanding that there *was* an epidemic when only
2,000 people out of ten million were infected.

The information story is full of examples of unperceived changes followed
by dislocating explosions. Those with the foresight to notice the explosion
just a little earlier than everyone else can reap huge benefits. Those who move
a little too slowly may be overwhelmed by the time they try to respond. Take
the case of digital photography.

In 1983, Christmas shoppers could buy digital cameras to hook up to their
IBM PC and Apple II home computers. The potential was there for anyone to
see; it was not hidden in secret corporate laboratories. But digital photography
did not take off. Economically and practically, it couldn’t. Cameras were
too bulky to put in your pocket, and digital memories were too small to hold
many images. Even 14 years later, film photography was still a robust industry.
In early 1997, Kodak stock hit a record price, with a 22% increase in
quarterly profit, “fueled by healthy film and paper sales…[and] its motion picture
film business,” according to a news report. The company raised its dividend
for the first time in eight years. But by 2007, digital memories had
become huge, digital processors had become fast and compact, and both were
cheap. As a result, cameras had become little computers. The company that
was once synonymous with photography was a shadow of its former self.
Kodak announced that its employee force would be cut to 30,000, barely a
fifth the size it was during the good times of the late 1980s. The move would
cost the company more than $3 billion. Moore’s Law moved faster than
Kodak did.

In the rapidly changing world of bits, it pays to notice even small changes,
and to do something about them.

Koan 6: Nothing Goes Away
-------------------------

2,000,000,000,000,000,000,000.

That is the number of bits that were created and stored away in 2007,
according to one industry estimate. The capacity of disks has followed its own
version of Moore’s Law, doubling every two or three years. For the time being
at least, that makes it possible to save everything though recent projections
suggest that by 2011, we may be producing more bits than we can store.

In financial industries, federal laws now require massive data retention, to
assist in audits and investigations of corruption. In many other businesses,
economic competitiveness drives companies to save everything they collect
and to seek out new data to retain. Wal-Mart stores have tens of millions of
transactions every day, and every one of them is saved—date, time, item,
store, price, who made the purchase, and how—credit, debit, cash, or gift card.
Such data is so valuable to planning the supply chain that stores will pay
money to get more of it from their customers. That is really what supermarket
loyalty cards provide—shoppers are supposed to think that the store is granting
them a discount in appreciation for their steady business, but actually the
store is paying them for information about their buying patterns. We might
better think of a privacy tax—we pay the regular price unless we want to keep
information about our food, alcohol, and pharmaceutical purchases from the
market; to keep our habits to ourselves, we pay extra.

The massive databases challenge our expectations about what will happen
to the data about us. Take something as simple as a stay in a hotel. When you
check in, you are given a keycard, not a mechanical key. Because the keycards
can be deactivated instantly, there is no longer any great risk associated
with losing your key, as long as you report it missing quickly. On the
other hand, the hotel now has a record, accurate to the second, of every time
you entered your room, used the gym or the business center, or went in the
back door after-hours. The same database could identify every cocktail and
steak you charged to the room, which other rooms you phoned and when, and
the brands of tampons and laxatives you charged at the hotel’s gift shop. This
data might be merged with billions like it, analyzed, and transferred to the
parent company, which owns restaurants and fitness centers as well as hotels.
It might also be lost, or stolen, or subpoenaed in a court case.

The ease of storing information has meant asking for more of it. Birth certificates
used to include just the information about the child’s and parents’
names, birthplaces, and birthdates, plus the parents’ occupations. Now the
electronic birth record includes how much the mother drank and smoked during
her pregnancy, whether she had genital herpes or a variety of other medical
conditions, and both parents’ social security numbers. Opportunities for
research are plentiful, and so are opportunities for mischief and catastrophic
accidental data loss.

.. epigraph::

    The data will all be kept
    forever, unless there are
    policies to get rid of it.

And the data will all be kept forever,
unless there are policies to get rid of it. For
the time being at least, the data sticks
around. And because databases are intentionally
duplicated—backed up for security,
or shared while pursuing useful analyses—it is far from certain that data can
ever be permanently expunged, even if we wish that to happen. The Internet
consists of millions of interconnected computers; once data gets out, there is
no getting it back. Victims of identity theft experience daily the distress of
having to remove misinformation from the record. It seems never to go away.

Koan 7: Bits Move Faster Than Thought 
-------------------------------------

The Internet existed before there were personal computers. It predates the
fiber optic communication cables that now hold it together. When it started
around 1970, the ARPANET, as it was called, was designed to connect a handful
of university and military computers. No one imagined a network connecting
tens of millions of computers and shipping information around the
world in the blink of an eye. Along with processing power and storage capacity,
networking has experienced its own exponential growth, in number of
computers interconnected and the rate at which data can be shipped over
long distances, from space to earth and from service providers into private
homes.

The Internet has caused drastic shifts in business practice. Customer service
calls are outsourced to India today not just because labor costs are low
there. Labor costs have *always* been low in India, but international telephone
calls used to be expensive. Calls about airline reservations and lingerie
returns are answered in India today because it now takes almost no time and
costs almost no money to send to India the bits representing your voice. The
same principle holds for professional services. When you are X-rayed at your
local hospital in Iowa, the radiologist who reads the X-ray may be half a
world away. The digital X-ray moves back and forth across the world faster
than a physical X-ray could be moved between floors of the hospital. When
you place an order at a drive-through station at a fast food restaurant, the
person taking the order may be in another state. She keys the order so it
appears on a computer screen in the kitchen, a few feet from your car, and
you are none the wiser. Such developments are causing massive changes to
the global economy, as industries figure out how to keep their workers in one
place and ship their business as bits.

In the bits world, in which messages flow instantaneously, it sometimes
seems that distance doesn’t matter at all. The consequences can be startling.
One of us, while dean of an American college, witnessed the shock of a father
receiving condolences on his daughter’s death. The story was sad but familiar,
except that this version had a startling twist. Father and daughter were
both in Massachusetts, but the condolences arrived from half-way around the
world before the father had learned that his daughter had died. News, even
the most intimate news, travels fast in the bits world, once it gets out. In the
fall of 2007, when the government of Myanmar suppressed protests by
Buddhist monks, television stations around the world showed video clips
taken by cell phone, probably changing the posture of the U.S. government.
The Myanmar rebellion also shows the power of information control when
information is just bits. The story dropped off the front page of the newspapers
once the government took total control of the Internet and cell phone
towers.

The instantaneous communication of massive amounts of information has
created the misimpression that there is a place called “Cyberspace,” a land
without frontiers where all the world’s people can be interconnected as
though they were residents of the same small town. That concept has been
decisively refuted by the actions of the world’s courts. National and state borders
still count, and count a lot. If a book is bought online in England, the
publisher and author are subject to British libel laws rather than those of the
homeland of the author or publisher. Under British law, defendants have to
prove their innocence; in the U.S., plaintiffs have to prove the guilt of the
defendants. An ugly downside to the explosion of digital information and its
movement around the world is that information may become less available
even where it would be legally protected (we return to this subject in Chapter
7, “You Can’t Say That on the Internet”). Publishers fear “libel tourism”—
lawsuits in countries with weak protection of free speech, designed to intimidate
authors in more open societies. It may prove simpler to publish only a
single version of a work for sale everywhere, an edition omitting information
that might somewhere excite a lawsuit.

Good and Ill, Promise and Peril
===============================

The digital explosion has thrown a lot of things up for grabs and we all have
a stake in who does the grabbing. The way the technology is offered to us, the
way we use it, and the consequences of the vast dissemination of digital information
are matters not in the hands of technology experts alone. Governments
and corporations and universities and other social institutions have a say. And
ordinary citizens, to whom these institutions are accountable, can influence
their decisions. Important choices are made every year, in government offices
and legislatures, in town meetings and police stations, in the corporate offices
of banks and insurance companies, in the purchasing departments of chain
stores and pharmacies. We all can help raise the level of discourse and understanding.
We can all help ensure that technical decisions are taken in a context
of ethical standards.

We offer two basic morals. The first is that information technology is inherently
neither good nor bad—it can be used for good or ill, to free us or to
shackle us. Second, new technology brings social change, and change comes
with both risks and opportunities. All of us, and all of our public agencies and
private institutions, have a say in whether technology will be used for good or
ill and whether we will fall prey to its risks or prosper from the opportunities
it creates.

Technology Is Neither Good nor Bad
----------------------------------

Any technology can be used for good or ill. Nuclear reactions create electric
power and weapons of mass destruction. The same encryption technology that
makes it possible for you to email your friends with confidence that no eavesdropper
will be able to decipher your message also makes it possible for terrorists
to plan their attacks undiscovered. The same Internet technology that
facilitates the widespread distribution of educational works to impoverished
students in remote locations also enables massive copyright infringement. The
photomanipulation tools that enhance your snapshots are used by child
pornographers to escape prosecution.

The key to managing the ethical and moral consequences of technology
while nourishing economic growth is to regulate the use of technology without
banning or *restricting its creation*.

It is a marvel that anyone with a smart cell phone can use a search engine
to get answers to obscure questions almost anywhere. Society is rapidly
being freed from the old limitations of geography and status in accessing
information. 

The same technologies can be used to monitor individuals, to track their
behaviors, and to control what information they receive. Search engines need
not return unbiased results. Many users of web browsers do not realize that
the sites they visit may archive their actions. Technologically, there could be
a record of exactly what you have been accessing and when, as you browse a
library or bookstore catalog, a site selling pharmaceuticals, or a service offering
advice on contraception or drug overdose. There are vast opportunities to
use this information for invasive but
relatively benign purposes, such as
marketing, and also for more questionable
purposes, such as blacklisting
and blackmail. Few regulations
mandate disclosure that the information
is being collected, or restrict the
use to which the data can be put.
Recent federal laws, such as the USA
PATRIOT Act, give government
agencies sweeping authority to sift
through mostly innocent data looking
for signs of “suspicious activity”
by potential terrorists—and to notice
lesser transgressions, such as
Governor Spitzer’s, in the process.
Although the World Wide Web now
reaches into millions of households,
the rules and regulations governing
it are not much better than those of
a lawless frontier town of the old
West.

.. sidebar:: BLACKLISTS AND WHITELISTS

    In the bits world, providers of services
    can create blacklists or
    whitelists. No one on a blacklist
    can use the service, but everyone
    else can. For example, an auctioneer
    might put people on a blacklist
    if they did not pay for their purchases.
    But service providers who
    have access to other information
    about visitors to their web sites
    might use undisclosed and far more
    sweeping criteria for blacklisting.
    A whitelist is a list of parties to
    whom services are available, with
    everyone else excluded. For example,
    a newspaper may whitelist its
    home delivery subscribers for
    access to its online content, allowing
    others onto the whitelist only
    after they have paid.

New Technologies Bring Both Risks and Opportunities
---------------------------------------------------

The same large disk drives that enable anyone with a home computer to analyze
millions of baseball statistics also allow anyone with access to confidential
information to jeopardize its security. Access to aerial maps via the
Internet makes it possible for criminals to plan burglaries of upscale houses,
but technologically sophisticated police know that records of such queries can
also be used to solve crimes.

Even the most un-electronic livelihoods are changing because of instant
worldwide information flows. There are no more pool hustlers today—journeymen
wizards of the cue, who could turn up in pool halls posing as outof-town
bumpkins just looking to bet on a friendly game, and walk away
with big winnings. Now when any newcomer comes to town and cleans up,
his name and face are on ``AZBilliards.com`` instantly for pool players everywhere
to see.

Social networking sites such as ``facebook.com``, ``myspace.com``, and
``match.com`` have made their founders quite wealthy. They have also given
birth to many thousands of new friendships, marriages, and other ventures.
But those pretending to be your online friends may not be as they seem.
Social networking has made it easier for predators to take advantage of the
naïve, the lonely, the elderly, and the young.

In 2006, a 13-year-old girl, Megan Meier of Dardenne Prairie, Missouri,
made friends online with a 16-year-old boy named “Josh.” When “Josh”
turned against her, writing “You are a bad person and everybody hates you….
The world would be a better place without you,” Megan committed suicide. Yet
Josh did not exist. Josh was a MySpace creation—but of whom? An early
police report stated that the mother of another girl in the neighborhood
acknowledged “instigating” and monitoring the account. That woman’s lawyer
later blamed someone who worked for his client. Whoever may have sent the
final message to Megan, prosecutors are having a hard time identifying any
law that might have been broken. “I can start MySpace on every single one of
you and spread rumors about every single one of you,” said Megan’s mother,
“and what’s going to happen to me? Nothing.” 

Along with its dazzling riches and vast horizons, the Internet has created
new manifestations of human evil—some of which, including the cyberharassment
Megan Meier suffered, may not be criminal under existing law. In
a nation deeply committed to free expression as a legal right, which Internet
evils should be crimes, and which are just wrong?

Vast data networks have made it possible to move work to where the
people are, not people to the work. The results are enormous business opportunities
for entrepreneurs who take advantage of these technologies and new
enterprises around the globe, and also the other side of the coin: jobs lost to
outsourcing.

The difference every one of us can make, to our workplace or to another
institution, can be to ask a question at the right time about the risks of
some new technological innovation—or to point out the possibility of doing
something in the near future that a few years ago would have been utterly
impossible.

----

We begin our tour of the digital landscape with a look at our privacy, a social
structure the explosion has left in shambles. While we enjoy the benefits of
ubiquitous information, we also sense the loss of the shelter that privacy once
gave us. And we don’t know what we want to build in its place. The good and
ill of technology, and its promise and peril, are all thrown together when
information about us is spread everywhere. In the post-privacy world, we
stand exposed to the glare of noonday sunlight—and sometimes it feels
strangely pleasant.