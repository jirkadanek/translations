===================
 About the Authors
===================

**Hal Abelson** is Class of 1922 Professor of Computer Science and Engineering
at MIT, and an IEEE Fellow. He has helped drive innovative educational technology
initiatives such MIT OpenCourseWare, cofounded Creative Commons
and Public Knowledge, and was founding director of the Free Software
Foundation. **Ken Ledeen**, Chairman/CEO of Nevo Technologies, has served on
the boards of numerous technology companies. **Harry Lewis**, former Dean of
Harvard College, is Gordon McKay Professor of Computer Science at Harvard
and Fellow of the Berkman Center for Internet and Society. He is author of
Excellence Without a Soul: Does Liberal Education Have a Future? Together,
the authors teach Quantitative Reasoning 48, an innovative Harvard course
on information for non-technical, non-mathematically oriented students