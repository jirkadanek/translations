.. Blown to Bits documentation master file, created by
   sphinx-quickstart on Fri Nov  6 14:04:35 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=============   
Blown to Bits
=============
-------------------------------------------------------------
Your Life, Liberty, and Happiness After the Digital Explosion
-------------------------------------------------------------

Copyright © 2008 Hal Abelson, Ken Ledeen, and Harry Lewis

This work is licensed under the Creative Commons Attribution-Noncommercial-Share Alike 3.0 United States License. To view a copy of this license visit http://creativecommons.org/licenses/by-nc-sa/3.0/us/ or send a letter to Creative Commons 171 Second Street, Suite 300, San Francisco, California, 94105, USA.

Contents
========

.. toctree::
   :maxdepth: 3
   
   Preface
   Acknowledgements
   About-the-Authors
   chapters
   Conclusion
   Appendix

   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

